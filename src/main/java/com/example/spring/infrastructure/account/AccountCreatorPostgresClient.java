package com.example.spring.infrastructure.account;

import com.example.spring.domain.account.AccountCreatorClient;
import com.example.spring.domain.model.account.Account;
import com.example.spring.domain.model.user.User;
import com.example.spring.infrastructure.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
class AccountCreatorPostgresClient implements AccountCreatorClient {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public void createPremium(long userId) {
        User user = userRepository.getOne(userId);
        user.addAccount(Account.createPremiumAccount());
    }

    @Override
    @Transactional
    public void createStandard(long userId) {
        User user = userRepository.getOne(userId);
        user.addAccount(Account.createStandardAccount());
    }

}

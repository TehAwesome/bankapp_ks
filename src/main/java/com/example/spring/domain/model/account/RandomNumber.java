package com.example.spring.domain.model.account;

import java.math.BigInteger;
import java.util.UUID;

class RandomNumber {
    static String generate() {

        String lUUID = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""), 16));
        String number = "4511000991" + lUUID.substring(0, 16);
        return number;
    }
}

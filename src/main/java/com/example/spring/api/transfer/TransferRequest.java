package com.example.spring.api.transfer;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@Builder
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public class TransferRequest {

    @NotBlank
    private long ownerId;

    @NotBlank
    private long ownerAccountId;

    @NotBlank
    private String clientAccountNumber;

    @NotBlank
    private BigDecimal amount;

    @NotBlank
    private String title;
}

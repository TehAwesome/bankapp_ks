package com.example.spring.domain.model.account;

public enum AccountType {
    PREMIUM, STANDARD
}

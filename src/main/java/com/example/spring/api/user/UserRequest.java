package com.example.spring.api.user;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;


@Builder
@Data
class UserRequest {

    @NotBlank
    String login;

    @Size(min = 7, max = 25)
    @NotBlank
    String password;

    @Email
    @NotBlank
    String email;

    LocalDate birthDate;


}

@Data
class PasswordRequest{
    @Size(min = 7, max = 25)
    @NotBlank
    private String oldPassword;

    @Size(min = 7, max = 25)
    @NotBlank
    private String newPassword;

}

@Data
class LoginRequest{
    @NotBlank
    private String login;
}

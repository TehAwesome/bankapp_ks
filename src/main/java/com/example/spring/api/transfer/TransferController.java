package com.example.spring.api.transfer;

import com.example.spring.domain.transfer.CreateTransferClient;
import com.example.spring.domain.transfer.TransferCommand;
import com.example.spring.domain.transfer.TransferCreator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/transfers")
@RequiredArgsConstructor
@Slf4j
public class TransferController {

    private final CreateTransferClient createTransferClient;
    private final TransferCreator transferCreator;
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public void createTransfer(@RequestBody TransferRequest transferRequest) {
//        log.info("Creating transfer from user with Id = " + transferRequest.getCustomerAccountId()
//                + ", to user with Id = " + transferRequest.getSellerAccountId());
//        TransferCommand transferCommand = TransferCommand.builder()
//                .customerAccountId(transferRequest.getCustomerAccountId())
//                .sellerAccountId(transferRequest.getSellerAccountId())
//                .amount(transferRequest.getAmount())
//                .build();
//        transferCreator.makeTransfer(transferRequest.getCustomerAccountId(), transferRequest.getSellerAccountId(), transferRequest.getAmount());
//        createTransferClient.createTransfer(transferCommand);
//
//    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransfer(@RequestBody TransferRequest transferRequest) {
        log.info("Creating transfer from client with account number = " + transferRequest.getClientAccountNumber()
                + ", to user with Id = " + transferRequest.getOwnerId());
        TransferCommand transferCommand = TransferCommand.builder()
                .ownerId(transferRequest.getOwnerId())
                .ownerAccountId(transferRequest.getOwnerAccountId())
                .clientAccountNumber(transferRequest.getClientAccountNumber())
                .amount(transferRequest.getAmount())
                .title(transferRequest.getTitle())
                .build();
        transferCreator.makeTransfer(transferRequest.getClientAccountNumber(), transferRequest.getOwnerAccountId(), transferRequest.getAmount());
        createTransferClient.createTransfer(transferCommand);

    }

    @GetMapping(path = "/pending")
    public void run() {
        transferCreator.run();
    }
}



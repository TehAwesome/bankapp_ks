package com.example.spring.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entered password does not match the old one!")
public class PasswordsNotMatchException extends RuntimeException {

}

package com.example.spring.api.card;

import com.example.spring.domain.card.CardHandlerClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;



@RestController
@RequestMapping("v1/cards")
@RequiredArgsConstructor
@Slf4j
public class CardController {

    private final CardHandlerClient cardHandlerClient;

    @PostMapping(path = "/accounts/{accountId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createCard(@Min(1) @PathVariable long accountId) {
        log.info("Creating card for account wih ID: {}", accountId);
        cardHandlerClient.createCard(accountId);
    }

}
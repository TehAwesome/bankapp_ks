package com.example.spring.domain.account;

import com.example.spring.domain.model.account.AccountType;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;


@Getter
@Builder
public class AccountCommand {

    private final String accountNumber;
    private final AccountType type;
    private BigDecimal fee;

    public boolean isPremium() {
        return AccountType.PREMIUM.equals(type);
    }
}

package com.example.spring.domain.model.card;

public enum Status {
    ACTIVE, RESTRICTED
}

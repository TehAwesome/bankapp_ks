package com.example.spring.domain.account;

public interface AccountCreatorClient {

    void createPremium(long userId);
    void createStandard(long userId);
}

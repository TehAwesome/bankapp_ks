package com.example.spring

import com.example.spring.api.account.AccountController
import com.example.spring.api.card.CardController
import com.example.spring.api.transfer.TransferController
import com.example.spring.api.user.UserController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = BankApplication.class)
class LoadContextTest extends Specification {

    @Autowired(required = false)
    private UserController userController

    @Autowired(required = false)
    private CardController cardController

    @Autowired(required = false)
    private TransferController transferController

    @Autowired(required = false)
    private AccountController accountController

    def "Should create all beans"() {
        expect:
        userController
        cardController
        transferController
        accountController
    }
}
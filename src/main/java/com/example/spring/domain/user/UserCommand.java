package com.example.spring.domain.user;

import com.example.spring.domain.model.user.User;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
public class UserCommand {
    private String login;
    private String password;
    private LocalDate birthDate;
    private String email;

    public User generateUser() {
        return User.builder()
                .birthDate(birthDate)
                .email(email)
                .login(login)
                .password(password)
                .build();
    }
}

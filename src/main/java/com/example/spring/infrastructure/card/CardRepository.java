package com.example.spring.infrastructure.card;

import com.example.spring.domain.model.card.Card;
import org.springframework.data.jpa.repository.JpaRepository;

interface CardRepository extends JpaRepository<Card, Long> {
}

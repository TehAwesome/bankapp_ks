package com.example.spring.domain.model.account;

import com.example.spring.domain.model.card.Card;
import com.example.spring.domain.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@ToString
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter
@Entity
@Builder
public class Account {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String accountNumber;
    private BigDecimal balance;
    private BigDecimal fee;

    @Enumerated(EnumType.STRING)
    private AccountType type;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @Setter
    private User user;

    public static Account createPremiumAccount() {
        return Account.builder()
                .accountNumber(RandomNumber.generate())
                .balance(BigDecimal.valueOf(10000))
                .fee(BigDecimal.ZERO)
                .type(AccountType.PREMIUM)
                .build();
    }

    public static Account createStandardAccount() {
        return Account.builder()
                .accountNumber(RandomNumber.generate())
                .balance(BigDecimal.valueOf(5000))
                .fee(BigDecimal.ZERO)
                .type(AccountType.STANDARD)
                .build();
    }

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private Set<Card> cardWallet = new HashSet<>();

    public void addCard(Card card) {
        cardWallet.add(card);
    }

//    @OneToMany(fetch = FetchType.LAZY)
//    private Set<Card> cards = new HashSet<>();

}

package com.example.spring.infrastructure.card;

import com.example.spring.domain.card.CardHandlerClient;
import com.example.spring.domain.model.account.Account;
import com.example.spring.domain.model.card.Card;
import com.example.spring.infrastructure.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CardHandlerPostgresClient implements CardHandlerClient {

    private final CardRepository cardRepository;
    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public void createCard(long accountId) {
        Account account = accountRepository.getOne(accountId);
        Card card = Card.createCard(account);
        account.addCard(card);
    }

}

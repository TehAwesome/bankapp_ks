package com.example.spring.domain.transfer;

import com.example.spring.api.transfer.TransferRequest;
import com.example.spring.domain.AuctionAppClient;
import com.example.spring.domain.exception.InsufficientFundsException;
import com.example.spring.domain.model.account.Account;
import com.example.spring.infrastructure.account.AccountRetrievalPostgresClient;
import com.example.spring.infrastructure.account.UpdateAccountPostgresClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class TransferCreator {

    private final AuctionAppClient auctionAppClient;
    private final AccountRetrievalPostgresClient accountRetrievalPostgresClient;
    private final UpdateAccountPostgresClient updateAccountPostgresClient;

    @Transactional
    public void makeTransfer(String clientAccountNumber, long sellerAccountId, BigDecimal amount) {
        Account sellerAccount = accountRetrievalPostgresClient.getAccountById(sellerAccountId).get();
        BigDecimal sellerAccountBalance = sellerAccount.getBalance();

        if (accountRetrievalPostgresClient.findAccountByAccountNumber(clientAccountNumber).isPresent()) {

            Account customerAccount = accountRetrievalPostgresClient.findAccountByAccountNumber(clientAccountNumber).get();
            long customerAccountId = customerAccount.getId();
            BigDecimal customerAccountBalance = customerAccount.getBalance();

            if (customerAccountBalance.subtract(amount).compareTo(BigDecimal.ZERO) >= 0) {
                updateAccountPostgresClient.updateAccountBalance(customerAccountId, customerAccountBalance.subtract(amount));
                updateAccountPostgresClient.updateAccountBalance(sellerAccountId, sellerAccountBalance.add(amount));
            } else {
                throw new InsufficientFundsException();
            }
        } else {
            updateAccountPostgresClient.updateAccountBalance(sellerAccountId, sellerAccountBalance.add(amount));
        }
    }

    public void run() {
        List<TransferRequest> transfers = auctionAppClient.execute();

//        for (TransferRequest transfer : transfers) {
//            makeTransfer(transfer.getClientAccountNumber(), transfer.getOwnerAccountId(), transfer.getAmount());
//        }
    }

}

package com.example.spring.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No account in the database with the specified Id!")
public class AccountNotFoundException extends RuntimeException {

}

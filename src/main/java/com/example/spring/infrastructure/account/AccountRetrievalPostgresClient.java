package com.example.spring.infrastructure.account;

import com.example.spring.domain.account.AccountRetrievalClient;
import com.example.spring.domain.model.account.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountRetrievalPostgresClient implements AccountRetrievalClient {

    private final AccountRepository accountRepository;


    @Override
    public Optional<Account> getAccountById(long accountId) {
        Optional<Account> account = accountRepository.findById(accountId);
        return account;
    }

    @Override
    public Optional<Account> findAccountByAccountNumber(String accountNumber) {
        Optional<Account> account = accountRepository.findByAccountNumber(accountNumber);
        return account;
    }


}

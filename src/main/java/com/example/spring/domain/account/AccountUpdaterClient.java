package com.example.spring.domain.account;

import java.math.BigDecimal;

public interface AccountUpdaterClient {
    void updateAccountBalance(long accountId, BigDecimal amount);
}

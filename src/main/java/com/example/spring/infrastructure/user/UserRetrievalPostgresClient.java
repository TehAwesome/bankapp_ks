package com.example.spring.infrastructure.user;

import com.example.spring.domain.exception.UserNotFoundException;
import com.example.spring.domain.model.user.User;
import com.example.spring.domain.user.UserResponse;
import com.example.spring.domain.user.UserRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserRetrievalPostgresClient implements UserRetrievalClient {

    private final UserRepository userRepository;

    @Override
    public List<UserResponse> findAllUser() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(User::generateResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse findById(long userId) {
        User user = getUserById(userId);
        return user.generateResponse();
    }

    @Override
    public User getUserById(long userId) {
        Optional<User> user = userRepository.findById(userId);
        return user.orElseThrow(UserNotFoundException::new);
    }
}

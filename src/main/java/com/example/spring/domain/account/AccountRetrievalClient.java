package com.example.spring.domain.account;

import com.example.spring.domain.model.account.Account;

import java.util.Optional;

public interface AccountRetrievalClient {
    Optional<Account> getAccountById(long accountId);

    Optional<Account> findAccountByAccountNumber(String accountNumber);
}

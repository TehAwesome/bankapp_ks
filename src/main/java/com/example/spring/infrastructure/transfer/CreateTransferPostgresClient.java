package com.example.spring.infrastructure.transfer;

import com.example.spring.domain.transfer.CreateTransferClient;
import com.example.spring.domain.transfer.TransferCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class CreateTransferPostgresClient implements CreateTransferClient {


    private final TransferRepository transferRepository;

    @Override
    @Transactional
    public void createTransfer(TransferCommand transferCommand) {
        transferRepository.save(transferCommand.generateTransfer());
    }
}

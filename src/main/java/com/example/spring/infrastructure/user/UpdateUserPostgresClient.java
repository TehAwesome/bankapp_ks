package com.example.spring.infrastructure.user;

import com.example.spring.domain.model.user.User;
import com.example.spring.domain.user.UpdateUserClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UpdateUserPostgresClient implements UpdateUserClient {

    private final UserRetrievalPostgresClient userRetrievalPostgresClient;
    @Override
    @Transactional
    public void updateLogin(long userId, String login) {
        User user = userRetrievalPostgresClient.getUserById(userId);
        user.setLogin(login);
    }

    @Override
    @Transactional
    public void changePassword(long userId, String password) {
        User user = userRetrievalPostgresClient.getUserById(userId);
        user.setPassword(password);
    }

    @Override
    @Transactional
    public void changeEmail(long userId, String email) {
        User user = userRetrievalPostgresClient.getUserById(userId);
        user.setEmail(email);
    }

}

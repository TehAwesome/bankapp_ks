package com.example.spring.domain.user;

public interface CreateUserClient {

    void create(UserCommand command);
}

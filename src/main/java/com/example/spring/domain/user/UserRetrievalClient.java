package com.example.spring.domain.user;

import com.example.spring.domain.model.user.User;

import java.util.List;

public interface UserRetrievalClient {

    List<UserResponse> findAllUser();

    UserResponse findById(long userId);

    User getUserById(long userId);
}

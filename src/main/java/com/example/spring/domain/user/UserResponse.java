package com.example.spring.domain.user;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

@Builder
@Value
public class UserResponse {

    @NonNull
    private final Long id;

    @NonNull
    private final String login;

    @NonNull
    private final LocalDate birthDate;
}

package com.example.spring.api.user;

import com.example.spring.domain.user.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("v1/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final CreateUserClient createUserClient;
    private final UserFacade userFacade;
    private final UserRetrievalClient userRetrievalClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@Valid @RequestBody UserRequest userRequest) {
        log.info("Creating user: {}", userRequest.getLogin());
        UserCommand userCommand = UserCommand.builder()
                .login(userRequest.getLogin())
                .password(userRequest.getPassword())
                .birthDate(userRequest.getBirthDate())
                .email(userRequest.getEmail())
                .build();
        createUserClient.create(userCommand);
    }


    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserResponse> findUserById(@Min(1) @PathVariable long userId) {
        log.info("Finding user: {}", userId);
        return ResponseEntity.ok(userRetrievalClient.findById(userId));
    }


    @GetMapping
    public ResponseEntity<List<UserResponse>> findUsers() {
        log.info("Finding all users");
        return ResponseEntity.ok(userRetrievalClient.findAllUser());
    }

    @PatchMapping(path = "/{userId}/update-login)")
    @ResponseStatus(HttpStatus.OK)
    public void updateLogin(@Min(1) @PathVariable long userId,
                            @RequestBody LoginRequest loginRequest) {
        log.info("Changing login for user with ID: {}", userId);
        userFacade.updateLogin(userId, loginRequest.getLogin());
    }

    @PatchMapping(path = "/{userId}/change-password")
    @ResponseStatus(HttpStatus.OK)
    public void changePassword(@Min(1) @PathVariable long userId,
                               @RequestBody PasswordRequest passwords) {
        log.info("Changing password for user with ID: {}", userId);
        userFacade.changePassword(userId, passwords.getOldPassword(), passwords.getNewPassword());
    }


}

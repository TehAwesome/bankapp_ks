package com.example.spring.api.account;

import com.example.spring.domain.account.AccountCreatorClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;


@RestController
@RequestMapping("v1/accounts")
@RequiredArgsConstructor
@Slf4j
public class AccountController {

    private final AccountCreatorClient accountCreatorClient;

    @PostMapping(path = "/users/{userId}/premium", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPremiumAccount(@Min(1) @PathVariable long userId) {
        log.info("Creating premium account for user: {}", userId);
        accountCreatorClient.createPremium(userId);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/users/{userId}/standard", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createStandardAccount(@Min(1) @PathVariable long userId) {
        log.info("Creating standard account for user: {}", userId);
        accountCreatorClient.createStandard(userId);
        return ResponseEntity.ok().build();
    }

}

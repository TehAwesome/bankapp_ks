package com.example.spring.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Not enough funds to realize the transfer!")
public class InsufficientFundsException extends RuntimeException {

}

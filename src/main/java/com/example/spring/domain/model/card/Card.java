package com.example.spring.domain.model.card;


import com.example.spring.domain.model.account.Account;
import lombok.*;

import javax.persistence.*;
import java.util.Random;

@Getter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Card {

    @Enumerated(EnumType.STRING)
    private Status status;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String lastFourDigits;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    @Setter
    private Account account;

    public static Card createCard(Account account) {
        Random random = new Random();
        Integer randomNumber = random.nextInt(8999) + 1000;
        String lastFourDigits = randomNumber.toString();

        return Card.builder()
                .lastFourDigits(lastFourDigits)
                .account(account)
                .status(Status.ACTIVE)
                .build();
    }

    void restrict() {
        this.status = Status.RESTRICTED;
    }

    boolean compareLastFourDigits(String lastFourDigits) {
        return this.lastFourDigits.equals(lastFourDigits);
    }

}

package com.example.spring.domain;

import com.example.spring.api.transfer.TransferRequest;

import java.util.List;

public interface AuctionAppClient {
    List<TransferRequest> execute();
}

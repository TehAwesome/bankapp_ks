package com.example.spring.infrastructure.account;

import com.example.spring.domain.account.AccountUpdaterClient;
import com.example.spring.domain.model.account.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class UpdateAccountPostgresClient implements AccountUpdaterClient {

    private final AccountRetrievalPostgresClient accountRetrievalPostgresClient;

    @Transactional
    @Override
    public void updateAccountBalance(long accountId, BigDecimal amount) {
        Account account = accountRetrievalPostgresClient.getAccountById(accountId).get();
        account.setBalance(amount);
    }

}

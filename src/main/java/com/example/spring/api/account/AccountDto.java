package com.example.spring.api.account;


import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class AccountDto {

    @NotBlank
    private String accountNumber;

    @NotBlank
    private String type;


}

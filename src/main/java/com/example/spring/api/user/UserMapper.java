package com.example.spring.api.user;


import com.example.spring.domain.model.user.User;

class UserMapper {

    static UserRequest mapToDto(User user) {
        return UserRequest.builder()
                .birthDate(user.getBirthDate())
                .login(user.getLogin())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }
}

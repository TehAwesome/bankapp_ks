package com.example.spring.domain.transfer;

import com.example.spring.domain.model.transfer.Transfer;
import lombok.Builder;

import java.math.BigDecimal;


@Builder
public class TransferCommand {

    private final long ownerId;
    private final long ownerAccountId;
    private final String clientAccountNumber;
    private final BigDecimal amount;
    private final String title;

    private final TransferCreator transferCreator;

    public Transfer generateTransfer() {
        return Transfer.builder()
                .amount(amount)
                .ownerId(ownerId)
                .ownerAccountId(ownerAccountId)
                .clientAccountNumber(clientAccountNumber)
                .title(title)
                .build();
    }
}

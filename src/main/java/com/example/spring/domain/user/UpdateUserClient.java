package com.example.spring.domain.user;

public interface UpdateUserClient {
    void updateLogin(long userId, String login);

    void changePassword(long userId, String password);

    void changeEmail(long userId, String email);
}

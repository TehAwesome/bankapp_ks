package com.example.spring.domain.user;

import com.example.spring.domain.exception.PasswordsNotMatchException;
import com.example.spring.infrastructure.user.UserRetrievalPostgresClient;
import lombok.Builder;
import lombok.NonNull;


@Builder
class PasswordChanger {

    @NonNull
    private UpdateUserClient updateUserClient;

    @NonNull
    private UserRetrievalPostgresClient userRetrievalPostgresClient;

    void changePassword(long userId, String oldPassword, String password) {
        if (userRetrievalPostgresClient.getUserById(userId).getPassword().equals(oldPassword)) {
            updateUserClient.changePassword(userId, password);
        } else {
            throw new PasswordsNotMatchException();
        }
    }
}

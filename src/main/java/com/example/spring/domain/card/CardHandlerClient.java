package com.example.spring.domain.card;

public interface CardHandlerClient {

    void createCard(long accountId);
}

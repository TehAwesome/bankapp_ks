package com.example.spring.api.user

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc

    @Autowired
    private ObjectMapper objectMapper

    def " Verify add user correctly "() {
        given:
        String personJsonObject = objectMapper.writeValueAsString(createUserRequest())

        when:
        MockHttpServletResponse result = mockMvc.perform(
                post("/v1/users")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(personJsonObject))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn().response

        mockMvc.perform(get("/v1/users/1"))
                .andDo(print())
                .andExpect(jsonPath("\$.login").value("MEMEMEME"))

        then:
        result.status == HttpStatus.CREATED.value()
    }

    UserRequest createUserRequest() {
        UserRequest userRequest = UserRequest.builder()
                .login("MEMEMEME")
                .password("1weqw124141")
                .birthDate(new LocalDate(2020, 10, 10))
                .email("asdasf@Ffa")
                .build()
        return userRequest
    }
}

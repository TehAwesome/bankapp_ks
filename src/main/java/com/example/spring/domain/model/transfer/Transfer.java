package com.example.spring.domain.model.transfer;

import lombok.Builder;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Builder
@Getter
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private long ownerId;
    private long ownerAccountId;
    private String clientAccountNumber;
    private BigDecimal amount;
    private String title;
}


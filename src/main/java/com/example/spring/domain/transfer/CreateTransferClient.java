package com.example.spring.domain.transfer;

public interface CreateTransferClient {
    void createTransfer(TransferCommand transferCommand);
}

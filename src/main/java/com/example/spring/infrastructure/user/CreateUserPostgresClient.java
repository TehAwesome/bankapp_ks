package com.example.spring.infrastructure.user;

import com.example.spring.domain.user.CreateUserClient;
import com.example.spring.domain.user.UserCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
class CreateUserPostgresClient implements CreateUserClient {

    private final UserRepository userRepository;

    @Transactional
    @Override
    public void create(UserCommand userCommand) {
        userRepository.save(userCommand.generateUser());
    }
}

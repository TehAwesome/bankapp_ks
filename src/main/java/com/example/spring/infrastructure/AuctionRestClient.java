package com.example.spring.infrastructure;

import com.example.spring.api.transfer.TransferRequest;
import com.example.spring.domain.AuctionAppClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class AuctionRestClient implements AuctionAppClient {

    private final String auctionAppHost;
    private final String purchaseRetrievalUri;
    private final RestTemplate restTemplate;


    public AuctionRestClient(@Value("${auction.app.host}") String auctionAppHost, @Value("${purchase.retrieval.uri}") String purchaseRetrievalUri,
                             RestTemplate restTemplate) {
        this.auctionAppHost = auctionAppHost;
        this.purchaseRetrievalUri = purchaseRetrievalUri;
        this.restTemplate = restTemplate;
    }


    @Override
    public List<TransferRequest> execute() {
        String url = auctionAppHost + purchaseRetrievalUri;
        ResponseEntity<List<TransferRequest>> exchange = restTemplate.exchange(
                url,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<TransferRequest>>() {
                }
        );
        return exchange.getBody();
    }
}


package com.example.spring.domain.model.user;

import com.example.spring.domain.model.account.Account;
import com.example.spring.domain.user.UserResponse;
import lombok.*;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@ToString
@Getter
@Entity
@Table(name = "USERS")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter
@Builder
@NamedQueries({
        @NamedQuery(name = "findUserById",
                query = "FROM User u where u.id = ?1"),
        @NamedQuery(name = "findByLogin",
                query = "FROM User WHERE login = :login")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence")
    private Long id;

    private String login;
    private String password;

    private LocalDate birthDate;
    private String email;

    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL)
    private Set<Account> accounts = new HashSet<>();

    public void addAccount(Account account) {
        accounts.add(account);
        account.setUser(this);
    }

    public UserResponse generateResponse() {
        return UserResponse.builder()
                .birthDate(birthDate)
                .login(login)
                .id(id)
                .build();
    }

}
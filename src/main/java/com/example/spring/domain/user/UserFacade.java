package com.example.spring.domain.user;

import com.example.spring.infrastructure.user.UserRetrievalPostgresClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final UpdateUserClient updateUserClient;
    private final UserRetrievalPostgresClient userRetrievalPostgresClient;

    public void updateLogin(long userId, String login) {
        updateUserClient.updateLogin(userId, login);
    }


    public void changePassword(long userId, String oldPassword, String password) {
        PasswordChanger passwordChanger = PasswordChanger.builder()
                .updateUserClient(updateUserClient)
                .userRetrievalPostgresClient(userRetrievalPostgresClient)
                .build();
        passwordChanger.changePassword(userId, oldPassword, password);
    }
}

package com.example.spring.infrastructure.transfer;

import com.example.spring.domain.model.transfer.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
}
